//
//  MovieManager.swift
//  Movie Collection
//
//  Created by R. Kukuh on 05/04/20.
//  Copyright © 2020 R. Kukuh. All rights reserved.
//

import Foundation

public class MovieManager {
    
    var movies: [Movie]
    
    init() {
        movies = [
            Movie(title: "The Invisible Man",
                  rating: 7.2,
                  actors: [
                    "Elisabeth Moss",
                    "Oliver Jackson-Cohen",
                    "Harriet Dyer"],
                  director: "Leigh Whannell",
                  durationInMinutes: 124,
                  posterAssetName: "Invisible Man"),
            
            Movie(title: "Bloodshot",
                  rating: 5.7,
                  actors: [
                    "Vin Diesel",
                    "Eiza González",
                    "Sam Heughan"],
                  director: " Dave Wilson",
                  durationInMinutes: 109,
                  posterAssetName: "Bloodshot"),
            
            Movie(title: "Bad Boys for Life",
                  rating: 6.9,
                  actors: [
                    "Will Smith",
                    "Martin Lawrence",
                    "Vanessa Hudgens"],
                  director: "Adil El Arbi & Bilall Fallah",
                  durationInMinutes: 124,
                  posterAssetName: "Bad Boys"),
            
            Movie(title: "Sonic the Hedgehog",
                  rating: 6.6,
                  actors: [
                    "Ben Schwartz",
                    "James Marsden",
                    "Jim Carrey"],
                  director: "Jeff Fowler",
                  durationInMinutes: 99,
                  posterAssetName: "Sonic")
            
        ]
    }
}
