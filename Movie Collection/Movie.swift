//
//  Movie.swift
//  Movie Collection
//
//  Created by R. Kukuh on 05/04/20.
//  Copyright © 2020 R. Kukuh. All rights reserved.
//

import Foundation

public struct Movie {
    
    var title: String
    var rating: Double
    var actors: [String]
    var director: String
    var durationInMinutes: Int
    var posterAssetName: String
    
}
