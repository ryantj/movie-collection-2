//
//  MoviesCollectionVC.swift
//  Movie Collection
//
//  Created by R. Kukuh on 05/04/20.
//  Copyright © 2020 R. Kukuh. All rights reserved.
//

import UIKit

class MoviesCollectionViewController: UICollectionViewController {
    
    var movies: [Movie]!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - UICollectionViewDataSource
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCell", for: indexPath) as! MovieCollectionViewCell
        
        let movie = movies[indexPath.row]
        
        cell.moviePoster.image = UIImage(named: movie.posterAssetName)
        cell.movieTitle.text = movie.title
    
        return cell
    }

}
