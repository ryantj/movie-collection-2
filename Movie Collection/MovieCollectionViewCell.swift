//
//  MovieCollectionViewCell.swift
//  Movie Collection
//
//  Created by R. Kukuh on 05/04/20.
//  Copyright © 2020 R. Kukuh. All rights reserved.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var moviePoster: UIImageView!
    @IBOutlet weak var movieTitle: UILabel!
    
}
